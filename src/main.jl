using Lindenmayer

output_d = joinpath(pwd(), "output")
icon = LSystem(Dict("F" => "F+Ft--F+F"), "F")

drawLSystem(icon, 
    forward         = 5,
    iterations      = 10,
    turn            = 90,
    filename        = joinpath(output_d, "lgflorentino-icon.png"),
    showpreview     = false
)
