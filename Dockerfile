FROM julia:1.9-bookworm

RUN apt-get -y update && apt-get -y upgrade

RUN apt-get -y install zsh

COPY . /lgflorentino-icons

WORKDIR /lgflorentino-icons

RUN julia -e "import Pkg; Pkg.activate(\".\"); Pkg.instantiate(); Pkg.precompile();"
