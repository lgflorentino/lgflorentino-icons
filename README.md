lgflorentino-icons
===


# Instructions 
Run once
```bash
docker build -t lgflorentino-icons_jl_img  -f Dockerfile .
```

Run 
```bash
./ctrlr run
```

Inspect output
```bash
gimp output/lgflorentino-icon.png
```
